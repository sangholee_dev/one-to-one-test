package practice.jpa.onetoone.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import practice.jpa.onetoone.entity.*;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
class OrderRepositoryTest {
    @Autowired
    MemberRepository memberRepository;

    @Autowired
    MemberDetailRepository memberDetailRepository;

    @Autowired
    MemberDetail2Repository memberDetail2Repository;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    QueryOrderRepository queryOrderRepository;

    @Autowired
    ProfileImageRepository profileImageRepository;

    @Autowired
    EntityManager entityManager;

    private Long orderId;

    @BeforeEach
    void setupData() {
        Member member = new Member("테스트");
        memberRepository.save(member);

        ProfileImage profileImage = new ProfileImage();
        profileImageRepository.save(profileImage);

        MemberDetail memberDetail = new MemberDetail();
        memberDetail.setContents("테스트1");
        memberDetail.setProfileImage(profileImage);
        memberDetailRepository.save(memberDetail);
        memberDetail.bindMember(member);

        MemberDetail2 memberDetail2 = new MemberDetail2();
        memberDetail2.setContents("테스트2");
        memberDetail2Repository.save(memberDetail2);
        memberDetail2.bindMember(member);

        Order order = new Order();
        orderRepository.save(order);
        order.setMember(member);

        orderId = order.getId();
        entityManager.flush();
        entityManager.clear();
    }

    @Test
    @DisplayName("fetch join 시 entity 내에 1:1 관계 lazy 적용 테스트")
    void getOrderSearchQuerydslTest_success() {
        Order findOrder = queryOrderRepository.getOrder(orderId);

        /*
         * 테스트 실행하면 Order 조회시 Member 를 fetch join 하면
         * Member 와 1:1관계인 MemberDetail 의  조회 쿼리가 즉시 호출됨
         * Member 와 1:N 관계인 MemberDetail2 는 조회 쿼리가 즉시 호출되지 않음
         */
        assertThat(findOrder.getMember().getMemberDetail().getContents()).isEqualTo("테스트1");
    }
}