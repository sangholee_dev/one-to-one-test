package practice.jpa.onetoone.entity;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class Member {
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "member")
    private MemberDetail memberDetail;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "member")
    private Set<MemberDetail2> memberDetail2 = new HashSet<>();

    public Member(String name) {
        this.name = name;
    }
}

