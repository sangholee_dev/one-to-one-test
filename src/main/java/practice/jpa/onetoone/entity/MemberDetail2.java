package practice.jpa.onetoone.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class MemberDetail2 {
    @Id
    @GeneratedValue
    private Long id;

    private String contents;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id")
    private Member member;

    public void bindMember(final Member member) {
        this.setMember(member);
        member.getMemberDetail2().add(this);
    }
}
