package practice.jpa.onetoone.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class MemberDetail {
    @Id
    @GeneratedValue
    private Long id;

    private String contents;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id")
    private Member member;

    @OneToOne(fetch = FetchType.LAZY)
    private ProfileImage profileImage;

    public void bindMember(final Member member) {
        this.setMember(member);
        member.setMemberDetail(this);
    }
}
