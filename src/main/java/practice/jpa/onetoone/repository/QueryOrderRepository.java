package practice.jpa.onetoone.repository;

import org.springframework.stereotype.Repository;
import practice.jpa.onetoone.entity.Order;

import java.util.List;

import static practice.jpa.onetoone.entity.QMember.member;
import static practice.jpa.onetoone.entity.QOrder.order;

@Repository
public class QueryOrderRepository extends QuerydslCustomRepositorySupport {
    public QueryOrderRepository() {
        super(Order.class);
    }

    public List<Order> getAllOrders() {
        return selectFrom(order)
                .join(order.member, member).fetchJoin()
                .fetch()
                ;
    }

    public Order getOrder(Long id) {
        return selectFrom(order)
                .join(order.member, member).fetchJoin()
                .where(order.id.eq(id))
                .fetchOne()
                ;
    }
}
