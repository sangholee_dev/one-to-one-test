package practice.jpa.onetoone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import practice.jpa.onetoone.entity.Member;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
