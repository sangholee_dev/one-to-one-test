package practice.jpa.onetoone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import practice.jpa.onetoone.entity.ProfileImage;

public interface ProfileImageRepository extends JpaRepository<ProfileImage, Long> {
}
