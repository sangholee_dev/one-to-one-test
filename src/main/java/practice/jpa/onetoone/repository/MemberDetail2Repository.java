package practice.jpa.onetoone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import practice.jpa.onetoone.entity.MemberDetail2;

public interface MemberDetail2Repository extends JpaRepository<MemberDetail2, Long> {
}
