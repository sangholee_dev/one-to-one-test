package practice.jpa.onetoone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import practice.jpa.onetoone.entity.Order;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query("select o from Order o " +
            "join fetch o.member m ")
    List<Order> getAllOrders();

    @Query("select o from Order o " +
            "join fetch o.member m " +
            "where o.id =:id")
    Optional<Order> getOrder(@Param("id") Long id);
}
